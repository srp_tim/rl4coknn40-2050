from rl4co.envs.tsp import TSPEnv
from rl4co.models.zoo.am.model import AttentionModel
from rl4co.utils.trainer import RL4COTrainer
import torch

# Environment, Model, and Lightning Module
env = TSPEnv(num_loc=20, train_file = "rl4co/data/data/tsp/tsp20_train_seed1111.npz", test_file =  "rl4co/data/data/tsp/tsp20_test_seed1234.npz", val_file= "rl4co/data/data/tsp/tsp20_val_seed4321.npz")
model = AttentionModel(env,
                       baseline="rollout",
                       optimizer_kwargs={'lr': 1e-4}
                       )

# Greedy rollouts over untrained model
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
td_init = env.reset(batch_size=[3]).to(device)
model = model.to(device)
out = model(td_init, phase="test", decode_type="greedy", return_actions=True)

# Plotting
print(f"Tour lengths: {[f'{-r.item():.2f}' for r in out['reward']]}")
for td, actions in zip(td_init, out['actions'].cpu()):
    env.render(td, actions)

# Training

trainer = RL4COTrainer(
    max_epochs=100,
    accelerator="gpu"
)


# Fit the model
trainer.fit(model)

# Test the model
trainer.test(model)

# Greedy rollouts over trained model (same states as previous plot)
model = model.to(device)
out = model(td_init, phase="test", decode_type="greedy", return_actions=True)

# Plotting
print(f"Tour lengths: {[f'{-r.item():.2f}' for r in out['reward']]}")
for td, actions in zip(td_init, out['actions'].cpu()):
    env.render(td, actions)
