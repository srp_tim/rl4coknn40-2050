#  GNU nano 4.8                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      attn50_gat.py                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 from rl4co.envs.tsp import TSPEnv
from rl4co.envs.tsp import TSPEnv
from rl4co.models.zoo import AttentionModel
from rl4co.utils.trainer import RL4COTrainer
import torch
from rl4co.models.nn.graph.gcn import GCNEncoder
from rl4co.models.nn.graph.mpnn import MessagePassingEncoder

import wandb

wandb.init(project="rl4co")

from lightning.pytorch.loggers import WandbLogger
logger = WandbLogger(project="rl4co", name="attn50_gat")

gcn_encoder = GCNEncoder(
    env_name='tsp',
    embedding_dim=128,
    num_nodes=100,
    num_layers=3,
)

mpnn_encoder = MessagePassingEncoder(
    env_name='tsp',
    embedding_dim=128,
    num_nodes=100,
    num_layers=3,
)
from lightning.pytorch.callbacks import ModelCheckpoint, RichModelSummary

# Print model summary
rich_model_summary = RichModelSummary(max_depth=3)

# Callbacks list
callbacks = [rich_model_summary]

# Environment, Model, and Lightning Module
env = TSPEnv(num_loc=50, batch_size=512)
model = AttentionModel(env,
                train_data_size=100_000,
                baseline="rollout",
                optimizer_kwargs={'lr': 1e-4}
                )

# Greedy rollouts over untrained model
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
td_init = env.reset(batch_size=[3]).to(device)
model = model.to(device)
out = model(td_init, phase="test", decode_type="greedy", return_actions=True)

# Plotting
print(f"Tour lengths: {[f'{-r.item():.2f}' for r in out['reward']]}")
for td, actions in zip(td_init, out['actions'].cpu()):
    env.render(td, actions)

# Training
trainer = RL4COTrainer(
    max_epochs=100,
    accelerator="gpu",
    logger=logger,
    callbacks=callbacks,
)

# Fit the model
trainer.fit(model)

# Test the model
trainer.test(model)

# Greedy rollouts over trained model (same states as previous plot)
model = model.to(device)
out = model(td_init, phase="test", decode_type="greedy", return_actions=True)

# Plotting
print(f"Tour lengths: {[f'{-r.item():.2f}' for r in out['reward']]}")
for td, actions in zip(td_init, out['actions'].cpu()):
    env.render(td, actions)
import numpy as np
np.random.seed(1234)

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
#model = AttentionModel.load_from_checkpoint("checkpoints/critic_50_epoch_epoch=063.ckpt", strict=False)
env = TSPEnv(num_loc=20)

# Generate data (100) and set as test dataset
new_dataset = env.dataset(10000)
dataloader = model._dataloader(new_dataset, batch_size=512)

model = model.to(device)
init_states = next(iter(dataloader))
td_init_generalization = env.reset(init_states).to(device)

out = model(td_init_generalization.clone(), phase="test", decode_type="greedy", return_actions=True)

# Plotting
#print(f"Tour lengths: {[f'{-r.item():.2f}' for r in out['reward']]}")
#for td, actions in zip(td_init_generalization, out['actions'].cpu()):
#    env.render(td, actions)
print("Testing on 20 nodes: ", out['reward'].mean())


#model = AttentionModel.load_from_checkpoint("checkpoints/critic_50_epoch_epoch=063.ckpt", strict=False)
env = TSPEnv(num_loc=50)

# Generate data (100) and set as test dataset
new_dataset = env.dataset(10000)
dataloader = model._dataloader(new_dataset, batch_size=512)

model = model.to(device)
init_states = next(iter(dataloader))
td_init_generalization = env.reset(init_states).to(device)

out = model(td_init_generalization.clone(), phase="test", decode_type="greedy", return_actions=True)

# Plotting
#print(f"Tour lengths: {[f'{-r.item():.2f}' for r in out['reward']]}")
#for td, actions in zip(td_init_generalization, out['actions'].cpu()):
#    env.render(td, actions)
print("Testing on 50 nodes: ", out['reward'].mean())

#model = AttentionModel.load_from_checkpoint("checkpoints/critic_50_epoch_epoch=063.ckpt", strict=False)
env = TSPEnv(num_loc=100)

# Generate data (100) and set as test dataset
new_dataset = env.dataset(10000)
dataloader = model._dataloader(new_dataset, batch_size=128)

model = model.to(device)
init_states = next(iter(dataloader))
td_init_generalization = env.reset(init_states).to(device)

out = model(td_init_generalization.clone(), phase="test", decode_type="greedy", return_actions=True)

# Plotting
#print(f"Tour lengths: {[f'{-r.item():.2f}' for r in out['reward']]}")
#for td, actions in zip(td_init_generalization, out['actions'].cpu()):
#    env.render(td, actions)
print("Testing on 100 nodes: ", out['reward'].mean())

#model = AttentionModel.load_from_checkpoint("checkpoints/critic_50_epoch_epoch=063.ckpt", strict=False)
env = TSPEnv(num_loc=500)

# Generate data (100) and set as test dataset
new_dataset = env.dataset(1000)
dataloader = model._dataloader(new_dataset, batch_size=8)

model = model.to(device)
init_states = next(iter(dataloader))
td_init_generalization = env.reset(init_states).to(device)

out = model(td_init_generalization.clone(), phase="test", decode_type="greedy", return_actions=True)

# Plotting
#print(f"Tour lengths: {[f'{-r.item():.2f}' for r in out['reward']]}")
#for td, actions in zip(td_init_generalization, out['actions'].cpu()):
#    env.render(td, actions)
print("Testing on 500 nodes: ", out['reward'].mean())

#model = AttentionModel.load_from_checkpoint("checkpoints/critic_50_epoch_epoch=063.ckpt", strict=False)
env = TSPEnv(num_loc=1000)

# Generate data (100) and set as test dataset
new_dataset = env.dataset(1000)
dataloader = model._dataloader(new_dataset, batch_size=2)

model = model.to(device)
init_states = next(iter(dataloader))
td_init_generalization = env.reset(init_states).to(device)

out = model(td_init_generalization.clone(), phase="test", decode_type="greedy", return_actions=True)

# Plotting
#print(f"Tour lengths: {[f'{-r.item():.2f}' for r in out['reward']]}")
#for td, actions in zip(td_init_generalization, out['actions'].cpu()):
#    env.render(td, actions)
print("Testing on 1000 nodes: ", out['reward'].mean())
wandb.finish()







