#!/usr/bin/env bash
#SBATCH --job-name=ppo20gat
#SBATCH --output=ppo20gat%j.log
#SBATCH --error=ppo20gat%j.err
#SBATCH --mail-user=alhermi@uni-hildesheim.de
#SBATCH --partition=STUD
#SBATCH --gres=gpu:1

#!/usr/bin/env bash


DEVICES="0"

wandb login 9815f3b0e8ce08b8878d8ca9a84bca3dd70f3978
wandb offline

CUDA_VISIBLE_DEVICES="$DEVICES" python ppo20_gat.py
























